<?php
add_action('custom_payment_process_returned_result', 'process_3rd_gateway_orders'); 
	function process_3rd_gateway_orders(){ 
if($_REQUEST['CXStransactionStatus'] === 'Created' ){
    $order = new WC_Order($_REQUEST['orderId']);
    $order->update_status('processing');
	  wp_redirect($_REQUEST['returnURL']);
	  // Remove cart
	  $woocommerce->cart->empty_cart();
      exit;
    
} else{
     $order = new WC_Order($_REQUEST['orderId']);
        $order->update_status('failed');
        $error = $_REQUEST['CXStransactionStatus'];
        $order->add_order_note( sprintf( "Payments Failed: '%s'", $error ) );
        wc_add_notice( $error, 'error' );
        wp_redirect( wc_get_checkout_url() );
        exit;
}  
        
}

class WC_Custom_Payment_Gateway extends WC_Payment_Gateway{
	public $api_data = array();
	public $data = array();

	public function __construct($child = false){
		$this->id = 'custom_payment';
		$this->method_title = __('Connexus Secure Payment ','woocommerce-custom-payment-gateway');
		$this->method_description = __( 'Allow customers to conveniently checkout directly with Coonexus Secure.','woocommerce-custom-payment-gateway');
		$this->title = __('Connexus Secure Payment','woocommerce-custom-payment-gateway');
		$this->has_fields = true;
		
		$this->order_button_text = __( 'Continue to payment', 'woocommerce' );

		$this->init_form_fields();
		$this->init_settings();


		$this->enabled = $this->get_option('enabled');
		$this->title = $this->get_option('title');
		$this->gateway_icon = $this->get_option('gateway_icon');
		//$this->icon = apply_filters( 'woocommerce_paypal_icon', 'https://test.connexuslab.com/assets/images/cxs-button.png'); // URL of the icon that will be displayed on
		$this->debug_mode = $this->get_option('debug_mode');


		$this->description = $this->get_option('description');
		$this->customer_note = $this->get_option('customer_note');

		$this->redirect_to_api_url = 'yes';

		$this->api_url_to_ping = $this->get_option('api_url_to_ping');

		$this->plugin_key =$this->get_option('plugin_key');

		// Debug mode, only administrators can use the gateway.
		if($this->debug_mode == 'yes'){
			if( !current_user_can('administrator') ){
				$this->enabled = 'no';
			}
		}

		add_action( 'woocommerce_receipt_custom_payment', array($this, 'receipt_page') );

		if($child === false){
			add_action('woocommerce_update_options_payment_gateways_'.$this->id, array($this, 'process_admin_options'));
		}

		add_action( 'woocommerce_api_' . strtolower( get_class( $this ) ), array( $this, 'process_returned_response' ) );
	}

	public function receipt_page( $order ){
		$request_body = $this->get_request_body( $this->api_data, $order );

		$customgateway_args_array = array();
		foreach ($request_body as $key => $value) {
			$customgateway_args_array[] = '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
		}
		echo '<form action="'.$this->api_url_to_ping.'" method="post" id="customgateway_payment_form">
				' . implode('', $customgateway_args_array) . '
			</form>';
		echo '<script>jQuery(document).ready(function(){
			jQuery("#customgateway_payment_form").submit();

		});</script>';
	}

	public function init_form_fields(){


				$this->form_fields = array(
					'enabled' => array(
						'title' 		=> __( 'Enable/Disable', 'woocommerce-custom-payment-gateway' ),
						'type' 			=> 'checkbox',
						'label' 		=> __( 'Enable Connexus Payment', 'woocommerce-custom-payment-gateway' ),
						'default' 		=> 'no'
					),
					'title' => array(
						'title' 		=> __( 'Method Title', 'woocommerce-custom-payment-gateway' ),
						'type' 			=> 'text',
						'description' 	=> __( 'The title of the gateway which will show to the user on the checkout page.', 'woocommerce-custom-payment-gateway' ),
						'default'		=> __( 'Connexus Secure Payment', 'woocommerce-custom-payment-gateway' ),
					),
					'plugin_key' => array(
                	    'title'=>'Plugin Key',
						'type' =>'text',
					),

					'gateway_icon' => array(
						'title' 		=> __( 'Gateway Icon', 'woocommerce-custom-payment-gateway' ),
						'type' 			=> 'text',
						'description' 	=> __( 'Icon URL for the gateway that will show to the user on the checkout page.', 'woocommerce-custom-payment-gateway' ),
						'default'		=> __( 'https://fg.cxs.bz/assets/images/cxs-button.png', 'woocommerce-custom-payment-gateway' ),
					),
					'description' => array(
						'title' => __( 'Customer Message', 'woocommerce-custom-payment-gateway' ),
						'css'	=> 'width:50%;',
						'type' => 'textarea',
						'default' => 'Use Connexus Secure to pay right from your bank !',
						'description' 	=> __( 'The message which you want it to appear to the customer on the checkout page.', 'woocommerce-custom-payment-gateway' ),

					),
					'customer_note' => array(
						'title' => __( 'Customer Note', 'woocommerce-custom-payment-gateway' ),
						'type' => 'textarea',
						'css'	=> 'width:50%;',
						'default' => '',
						'description' 	=> __( 'A note for the customer after the Checkout process.', 'woocommerce-custom-payment-gateway' ),

					),
					'api_url_to_ping' => array(
						'title'       => __( 'API URL', 'woocommerce-custom-payment-gateway' ),
						'type'        => 'text',
						'description' => __( 'The gateway will send the payment data to this URL after placing the order.', 'woocommerce-custom-payment-gateway' ),
						'default'     => 'https://api.connexus.fi/apiv1/bridge',
					),

					'debug_mode' => array(
						'title' 		=> __( 'Enable Debug Mode', 'woocommerce-custom-payment-gateway' ),
						'type' 			=> 'checkbox',
						'label' 		=> __( 'Enable ', 'woocommerce-custom-payment-gateway' ),
						'default' 		=> 'no',
						'description'	=> __('If debug mode is enabled, the payment gateway will be activated just for the administrator. You can use the debug mode to make sure that the gateway work as you expected.'),
					),
			 );



	}



	/**
	 * Admin Panel Options
	 * - Options for bits like 'title' and availability on a country-by-country basis
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function admin_options() {

		?>
		<h3><?php _e( 'Connexus Payment Settings', 'woocommerce-custom-payment-gateway' ); ?></h3>
			<div id="poststuff">
				<div id="post-body" class="metabox-holder columns-2">
					<div id="post-body-content">
						<table class="form-table">
							<?php $this->generate_settings_html();?>
						</table><!--/.form-table-->
					</div>
                    </div>
				</div>
				<div class="clear"></div>

		<?php
	}

	public function process_payment( $order_id ) {
		global $woocommerce;

		$order = new WC_Order( $order_id );

		update_post_meta((int)$order_id, 'woocommerce_customized_payment_data', $this->data);
		update_post_meta((int)$order_id, 'woocommerce_customized_customer_note', $this->customer_note);


		// Update order status
	    $order->update_status('wc-pending');

		// Reduce stock levels
		//$order->reduce_order_stock(); deprecated since 3.0
		wc_reduce_stock_levels( $order_id );

		if(trim($this->customer_note) != ''){
			$order->add_order_note($this->customer_note, 1);
		}
		// Remove cart
		//$woocommerce->cart->empty_cart();

		// ping to URL.

		if( $this->api_url_to_ping ){
			$is_return = $this->ping_api( $this->api_data, $order_id, $order );
			if(isset($is_return['redirect'])){
				return array(
					'result' => 'success',
					'redirect' => $is_return['redirect']
				);
			}
		}

		// Return thankyou redirect
		return array(
			'result' => 'success',
			'redirect' => $this->get_return_url( $order )
		);
	}

public function get_request_body( $api_data, $order_id ){
	$request_body = array(
                'pluginKey'=>$this->plugin_key,
                );
	if(!empty($api_data)){
		$request_body = array_merge($api_data, $request_body);
	}
		$order = new WC_Order($order_id);
		$wc_data = array(
					'orderId' => $order->get_id(),
					'amount' => $order->get_total(),
					'firstName' => $order->get_billing_first_name(),
					'lastName' => $order->get_billing_last_name(),
					'company' => $order->get_billing_company(),
					'postcode' => $order->get_billing_postcode(),
					'city' => $order->get_billing_city(),
					'state' => $order->get_billing_state(),
					'country' => $order->get_billing_country(),
					'email' => $order->get_billing_email(),
					'phone' => $order->get_billing_phone(),
					'ipAddress' => $order->get_customer_ip_address(),
					'returnURL' => $this->get_return_url( $order ),
					'merchantTrackingNumber'=>$order->get_id(),
					);
	return array_merge($wc_data, $request_body);
}
public function ping_api( $api_data, $order_id, $order = false ){
	$request_body = $this->get_request_body( $api_data, $order_id );

	if($this->redirect_to_api_url == 'yes'){
			return array('redirect' =>  $order->get_checkout_payment_url( true ) );
	}else{
		   $response = wp_remote_post( $this->api_url_to_ping, array(
				    'method' => strtoupper('post'),
				    'body' => $request_body,
			    )
		    );
	}


}

    public function get_icon(){
		return (trim($this->gateway_icon) != '')?'<img class="customized_payment_icon" src="'.  esc_attr($this->gateway_icon) .'" />':'';
	}

	/**
	 * For developers to process returned URLs from 3rd-party gateways
     * @since 1.3.8
	 */
	public function process_returned_response(){
        do_action('custom_payment_process_returned_result');
        exit;
    }

}
