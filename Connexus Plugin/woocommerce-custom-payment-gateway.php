<?php
/* @wordpress-plugin
 * Plugin Name:       Connexus Secure Payment
 * Description:       Connexus payment gateway.
 * Version:           1.3.8
 * Author:            Mahender Kasarla
 * Text Domain:       woocommerce-connexus-payment-gateway
 * Domain Path:       /languages
 */



if(custom_gateway_is_woocommerce_active()){

	add_filter('woocommerce_payment_gateways', 'add_custom_payment_gateway');
	function add_custom_payment_gateway( $gateways ){
		$gateways[] = 'WC_Custom_Payment_Gateway';
		return $gateways;
	}

	add_action('plugins_loaded', 'init_custom_payment_gateway');
	function init_custom_payment_gateway(){
		require_once 'class-woocommerce-custom-payment-gateway.php';
	}

	add_action( 'admin_init', 'custom_payment_admin_css' );
	function custom_payment_admin_css() {
       	wp_enqueue_style( 'custom_payment_admin_css', plugins_url('includes/assets/css/admin.css', __FILE__) );
   	}
}
add_action('woocommerce_thankyou', 'connexus_custompayment_add_customer_note_to_thank_you_page');
function connexus_custompayment_add_customer_note_to_thank_you_page($order_id){
	$customer_note =	get_post_meta((int)$order_id, 'woocommerce_customized_customer_note', true);
	if($customer_note){
		echo '<p>'	. $customer_note . '</p>';
	}
}

function custom_gateway_is_woocommerce_active(){
	$active_plugins = (array) get_option( 'active_plugins', array() );

	if ( is_multisite() )
		$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );

	return in_array( 'woocommerce/woocommerce.php', $active_plugins ) || array_key_exists( 'woocommerce/woocommerce.php', $active_plugins );
}
